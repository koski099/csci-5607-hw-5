from math import sqrt
from random import random

GRID_X = 8
GRID_Y = 8


def dist(p1, p2):
    return sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2)


def mid(p1, p2):
    return (p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2


def midpoints(p1, p2, p3, p4):
    return mid(p1, p2), mid(p1, p3), mid(p2, p4), mid(p3, p4), mid(p1, p4)


def shrink(goal, p1, p2, p3, p4):
    m1, m2, m3, m4, m5 = midpoints(p1, p2, p3, p4)

    # Upper left corner
    if goal == p1:
        return p1, m1, m2, m5

    # Upper right corner
    elif goal == p2:
        return m1, p2, m5, m3

    # Lower left corner
    elif goal == p3:
        return m2, m5, p3, m4

    # Lower right corner
    elif goal == p4:
        return m5, m3, m4, p4

    # This shouldn't happen
    else:
        return (0, 0), (0, 0), (0, 0), (0, 0)


def fractize(grid, p1=None, p2=None, p3=None, p4=None, random_factor=0.2):
    if p1 is None:
        p1 = (0, 0)

    if p2 is None:
        p2 = (0, grid.shape[1] - 1)

    if p3 is None:
        p3 = (grid.shape[0] - 1, 0)

    if p4 is None:
        p4 = (grid.shape[0] - 1, grid.shape[1] - 1)

    m1, m2, m3, m4, m5 = midpoints(p1, p2, p3, p4)

    if dist(p1, p2) > 1:
        grid[m1] = (grid[p1] + grid[p2]) / 2 + random_factor * random() - random_factor / 2
        grid[m2] = (grid[p1] + grid[p3]) / 2 + random_factor * random() - random_factor / 2
        grid[m3] = (grid[p2] + grid[p4]) / 2 + random_factor * random() - random_factor / 2
        grid[m4] = (grid[p3] + grid[p4]) / 2 + random_factor * random() - random_factor / 2
        grid[m5] = (grid[p1] + grid[p2] + grid[p3] + grid[p4]) / 4 + random_factor * random() - random_factor / 2

        fractize(grid, *shrink(p1, p1, p2, p3, p4), random_factor=random_factor/2)
        fractize(grid, *shrink(p2, p1, p2, p3, p4), random_factor=random_factor/2)
        fractize(grid, *shrink(p3, p1, p2, p3, p4), random_factor=random_factor/2)
        fractize(grid, *shrink(p4, p1, p2, p3, p4), random_factor=random_factor/2)


if __name__ == '__main__':
    print shrink((2, 0), (2, 0), (2, 2), (4, 0), (4, 2))