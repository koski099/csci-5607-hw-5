from math import floor, ceil, sqrt
import numpy as np
import OpenGL.GL as gl
from vispy.gloo import Program, IndexBuffer, VertexBuffer, Texture2D
from vispy.io import imread
from HW5.fractize import fractize


vertex = """
#version 150 core
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

attribute vec3 position;
attribute vec2 texuv;

out vec2 texcoord;

void main() {
    gl_Position = projection * view * model * vec4(position, 1.0);
    texcoord = texuv;
}
"""

fragment = """
#version 150 core

uniform sampler2D bricks;

in vec2 texcoord;

void main() {
   gl_FragColor = vec4(texture(bricks, texcoord).rgb, 1);
}
"""


def get_buildings_program(GRID_X=33, GRID_Y=33):
    GRID_X -= 1
    GRID_Y -= 1

    FACES = 4

    # Create an array big enough for all of the generated terrain,
    # plus a surrounding flat terrain.
    V = np.zeros(6 * FACES, [
        ("position", np.float32, 3),
        ("texuv", np.float32, 2),
    ])

    # Face 1
    V['position'][0] = (GRID_X/2, -GRID_Y/2,  GRID_X/4)
    V['position'][1] = (GRID_X/2, -GRID_Y/2,       0.0)
    V['position'][2] = (GRID_X/2,  GRID_Y/2,       0.0)
    V['position'][3] = (GRID_X/2, -GRID_Y/2,  GRID_X/4)
    V['position'][4] = (GRID_X/2,  GRID_Y/2,  GRID_X/4)
    V['position'][5] = (GRID_X/2,  GRID_Y/2,       0.0)
    
    V['texuv'][0] = (0.0,           0.0)
    V['texuv'][1] = (0.0,      GRID_Y/16)
    V['texuv'][2] = (GRID_X/4, GRID_Y/16)
    V['texuv'][3] = (0.0,           0.0)
    V['texuv'][4] = (GRID_X/4,      0.0)
    V['texuv'][5] = (GRID_X/4, GRID_Y/16)

    # Face 2
    V['position'][6] = (-GRID_X/2, -GRID_Y/2,  GRID_X/4)
    V['position'][7] = (-GRID_X/2, -GRID_Y/2,       0.0)
    V['position'][8] = (-GRID_X/2,  GRID_Y/2,       0.0)
    V['position'][9] = (-GRID_X/2, -GRID_Y/2,  GRID_X/4)
    V['position'][10] = (-GRID_X/2,  GRID_Y/2,  GRID_X/4)
    V['position'][11] = (-GRID_X/2,  GRID_Y/2,       0.0)

    V['texuv'][6] = (0.0,             0.0)
    V['texuv'][7] = (0.0,       GRID_Y/16)
    V['texuv'][8] = (GRID_X/4,  GRID_Y/16)
    V['texuv'][9] = (0.0,             0.0)
    V['texuv'][10] = (GRID_X/4,       0.0)
    V['texuv'][11] = (GRID_X/4, GRID_Y/16)

    # Face 3
    V['position'][12] = (-GRID_X/2, GRID_Y/2,  GRID_X/4)
    V['position'][13] = (-GRID_X/2, GRID_Y/2,       0.0)
    V['position'][14] = (+GRID_X/2, GRID_Y/2,       0.0)
    V['position'][15] = (-GRID_X/2, GRID_Y/2,  GRID_X/4)
    V['position'][16] = (+GRID_X/2, GRID_Y/2,  GRID_X/4)
    V['position'][17] = (+GRID_X/2, GRID_Y/2,       0.0)

    V['texuv'][12] = (0.0,             0.0)
    V['texuv'][13] = (0.0,       GRID_Y/16)
    V['texuv'][14] = (GRID_X/4,  GRID_Y/16)
    V['texuv'][15] = (0.0,             0.0)
    V['texuv'][16] = (GRID_X/4,       0.0)
    V['texuv'][17] = (GRID_X/4, GRID_Y/16)

    # Face 4
    V['position'][18] = (-GRID_X/2, -GRID_Y/2,  GRID_X/4)
    V['position'][19] = (-GRID_X/2, -GRID_Y/2,       0.0)
    V['position'][20] = (+GRID_X/2, -GRID_Y/2,       0.0)
    V['position'][21] = (-GRID_X/2, -GRID_Y/2,  GRID_X/4)
    V['position'][22] = (+GRID_X/2, -GRID_Y/2,  GRID_X/4)
    V['position'][23] = (+GRID_X/2, -GRID_Y/2,       0.0)

    V['texuv'][18] = (0.0,             0.0)
    V['texuv'][19] = (0.0,       GRID_Y/16)
    V['texuv'][20] = (GRID_X/4,  GRID_Y/16)
    V['texuv'][21] = (0.0,             0.0)
    V['texuv'][22] = (GRID_X/4,       0.0)
    V['texuv'][23] = (GRID_X/4, GRID_Y/16)

    vertices = VertexBuffer(V)

    I = list(range(6 * FACES))

    # Build program
    # --------------------------------------
    program = Program(vertex, fragment)

    program.bind(vertices)

    bricks = imread('bricks.jpg')
    program['bricks'] = Texture2D(bricks)
    program['bricks'].wrapping = gl.GL_REPEAT

    program._indices = IndexBuffer(I)
    program._mode = gl.GL_TRIANGLES

    return program