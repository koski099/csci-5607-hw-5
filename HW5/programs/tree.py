from math import sqrt
from random import random
import numpy as np
import OpenGL.GL as gl
from vispy.gloo import VertexBuffer, IndexBuffer, Program, Texture2D
from vispy.io import imread

vertex = """
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

varying vec2 texcoord;

attribute vec3 position;
attribute vec2 texuv;


void main() {
    //v_color = vec4(0.46, 0.66, 0.07, 1);
    texcoord = texuv;
    gl_Position = projection * view * model * vec4(position,1.0);
}
"""

fragment = """
varying vec2 texcoord;

uniform sampler2D bark;

void main() {
    //gl_FragColor = v_color;
    gl_FragColor = texture(bark, texcoord).rgba;
}
"""


def subtract(p2, p1):
    direction = p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2]
    length = sqrt(direction[0] ** 2 + direction[1] ** 2 + direction[2] ** 2)

    return direction[0] / length, direction[1] / length, direction[2] / length


def get_random(factor):
    #return factor + (random() * factor - factor / 2)
    return factor


def grow(word, points, indices, colors, base, original_length=None, old_base=None):
    new_base = len(points)
    length = len(word)
    base_point = points[base]
    base_direction = subtract(base_point, points[dict(i[::-1] for i in indices)[base]])

    if not length:
        return

    if original_length is None:
        original_length = length

    letter = word[0]

    ratio = 1.0 * float(length) / original_length
    
    branching_factor = 0.7

    if letter == 'a':
        new_point = (
            base_point[0] + 0.1 * ratio * base_direction[0],
            base_point[1] + 0.1 * ratio * base_direction[1],
            base_point[2] + 0.1 * ratio * base_direction[2])
    elif letter == 'b':
        new_point = (
            base_point[0] + ratio * (base_direction[0] + get_random(branching_factor)),
            base_point[1] + ratio * (base_direction[1] + get_random(branching_factor)),
            base_point[2] + ratio * base_direction[2])
    elif letter == 'c':
        new_point = (
            base_point[0] + ratio * (base_direction[0] + get_random(branching_factor)),
            base_point[1] + ratio * (base_direction[1] - get_random(branching_factor)),
            base_point[2] + ratio * base_direction[2])
    elif letter == 'd':
        new_point = (
            base_point[0] + ratio * (base_direction[0] - get_random(branching_factor)),
            base_point[1] + ratio * (base_direction[1] + get_random(branching_factor)),
            base_point[2] + ratio * base_direction[2])
    elif letter == 'e':
        new_point = (
            base_point[0] + ratio * (base_direction[0] - branching_factor),
            base_point[1] + ratio * (base_direction[1] - branching_factor),
            base_point[2] + ratio * base_direction[2])
    elif letter == '[':
        return grow(word[1:], points, indices, colors, base, original_length=original_length, old_base=base)
    elif letter == ']':
        return grow(word[1:], points, indices, colors, old_base, original_length=original_length, old_base=old_base)
    else:
        print 'lol wut: ', letter
        new_point = (
            base_point[0] + base_direction[0] + ratio,
            base_point[1] + base_direction[1] + ratio,
            base_point[2] + base_direction[2] + ratio)

    indices.append((base, new_base))
    points.append(new_point)

    if len(word) > 1:
        colors.append((0.6, 0.98, 0.6) if word[1] == ']' else (0.63, 0.32, 0.18))

    if len(word) == 1 and word[0] != ']':
        colors.append((0.6, 0.98, 0.6))

    grow(word[1:], points, indices, colors, new_base, original_length=original_length, old_base=old_base)


def get_tree_program(start=(0, 0, 0), word='abcabc'):
    points = [start, (start[0], start[1], start[2] + 1.0)]
    indices = [(0, 1)]
    colors = [(0.63, 0.32, 0.18)] * 2

    grow(word, points, indices, colors, 1)

    vertices = []
    new_indices = []
    texuvs = []

    for i, index in enumerate(indices):
        segment_points = [
            # Face 1
            list(points[index[1]]),
            list(points[index[0]]),
            list(points[index[0]]),
            list(points[index[1]]),
            list(points[index[1]]),
            list(points[index[0]]),

            # Face 2
            list(points[index[1]]),
            list(points[index[0]]),
            list(points[index[0]]),
            list(points[index[1]]),
            list(points[index[1]]),
            list(points[index[0]]),

            # Face 3
            list(points[index[1]]),
            list(points[index[0]]),
            list(points[index[0]]),
            list(points[index[1]]),
            list(points[index[1]]),
            list(points[index[0]]),

            # Face 4
            list(points[index[1]]),
            list(points[index[0]]),
            list(points[index[0]]),
            list(points[index[1]]),
            list(points[index[1]]),
            list(points[index[0]]),
        ]

        if points[index[0]][:2] == points[index[1]][:2]:
            width1 = min(0.03 * 2 / (segment_points[0][2] - points[0][2]), 0.03)
            width2 = min(0.03 * 2 / ((segment_points[1][2] - points[0][2]) or 1), 0.03)
        else:
            width1 = 0.1 * 0.2 / (segment_points[0][2] - points[0][2])
            width2 = 0.1 * 0.2 / (segment_points[1][2] - points[0][2])

        # For each trunk/branch, create a rectangular prism around it
        # by adding 4 faces, one for each of NESW

        # Face 1
        segment_points[0][0] -= width1
        segment_points[1][0] -= width2
        segment_points[2][0] += width2
        segment_points[3][0] += width1
        segment_points[4][0] -= width1
        segment_points[5][0] += width2

        segment_points[0][1] += width1
        segment_points[1][1] += width2
        segment_points[2][1] += width2
        segment_points[3][1] += width1
        segment_points[4][1] += width1
        segment_points[5][1] += width2

        # Face 2
        segment_points[6][0] -= width1
        segment_points[7][0] -= width2
        segment_points[8][0] += width2
        segment_points[9][0] += width1
        segment_points[10][0] -= width1
        segment_points[11][0] += width2

        segment_points[6][1] -= width1
        segment_points[7][1] -= width2
        segment_points[8][1] -= width2
        segment_points[9][1] -= width1
        segment_points[10][1] -= width1
        segment_points[11][1] -= width2

        # Face 3
        segment_points[12][1] -= width1
        segment_points[13][1] -= width2
        segment_points[14][1] += width2
        segment_points[15][1] += width1
        segment_points[16][1] -= width1
        segment_points[17][1] += width2

        segment_points[12][0] -= width1
        segment_points[13][0] -= width2
        segment_points[14][0] -= width2
        segment_points[15][0] -= width1
        segment_points[16][0] -= width1
        segment_points[17][0] -= width2

        # Face 4
        segment_points[18][1] -= width1
        segment_points[19][1] -= width2
        segment_points[20][1] += width2
        segment_points[21][1] += width1
        segment_points[22][1] -= width1
        segment_points[23][1] += width2

        segment_points[18][0] += width1
        segment_points[19][0] += width2
        segment_points[20][0] += width2
        segment_points[21][0] += width1
        segment_points[22][0] += width1
        segment_points[23][0] += width2

        vertices += segment_points

        new_indices += [24*i + n for n in range(24)]

        texuvs += 4 * [
            (0.0, 0.0),
            (0.0, 1.0),
            (1.0, 1.0),
            (1.0, 0.0),
            (0.0, 0.0),
            (1.0, 1.0),
        ]



    V = np.zeros(len(vertices), [
        ("position", np.float32, 3),
        ("texuv", np.float32, 2),
    ])

    V["position"] = np.array(vertices)
    V["texuv"] = np.array(texuvs)

    # Build program
    # --------------------------------------
    program = Program(vertex, fragment)

    program.bind(VertexBuffer(V))

    bark = imread('bark.jpg')
    program['bark'] = Texture2D(bark)
    program['bark'].wrapping = gl.GL_REPEAT

    program._indices = IndexBuffer(np.array(new_indices))
    program._mode = gl.GL_TRIANGLES

    return program