from math import floor, ceil, sqrt
import numpy as np
import OpenGL.GL as gl
from vispy.gloo import Program, IndexBuffer, VertexBuffer, Texture2D
from vispy.io import imread
from HW5.fractize import fractize


vertex = """
#version 150 core
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec4 color;
varying vec4 v_color;
const vec3 inLightDir = normalize(vec3(0, 1, -0.5));

attribute vec3 position;
attribute vec2 inTexcoord1;
attribute vec2 inTexcoord2;
attribute vec3 inNormal;

out vec2 texcoord1;
out vec2 texcoord2;
out vec3 pos;
out vec3 lightDir;
out vec3 normal;

void main() {
    v_color = color;
    gl_Position = projection * view * model * vec4(position, 1.0);
    texcoord1 = inTexcoord1;
    texcoord2 = inTexcoord2;

    pos = (view * model * vec4(position, 1.0)).xyz;
    lightDir = (view * vec4(inLightDir, 0.0)).xyz;
    vec4 norm4 = transpose(inverse(view * model)) * vec4(inNormal, 0.0);
    normal = normalize(norm4.xyz);
}
"""

fragment = """
#version 150 core
varying vec4 v_color;

uniform sampler2D grass1;
uniform sampler2D grass2;
uniform sampler2D ground;

in vec2 texcoord1;
in vec2 texcoord2;
in vec3 pos;
in vec3 normal;
in vec3 lightDir;

attribute vec3 position;

const float ambient = 0.3;

void main() {
    //gl_FragColor = v_color;

    vec3 color;

    color = 0.5 * texture(grass1, texcoord1).rgb + 0.5 * texture(grass2, texcoord2).rgb;

    vec3 diffuseC = color * max(dot(-lightDir, normal), 0.0);

    vec3 ambC = ambient * color;

    vec3 viewDir = normalize(-pos);
    vec3 reflectDir = reflect(viewDir, normal);
    float spec = max(dot(reflectDir, lightDir), 0.0);

    if (dot(-lightDir, normal) <= 0.0) {
        spec = 0;
    }

    vec3 specC = 0.0 * vec3(0.5, 0.5, 0.5) * pow(spec, 20);

    vec3 oColor = ambC + diffuseC;// + specC;

    gl_FragColor = vec4(oColor, 1);
}
"""


def interpolate_grid(grid, x, y, default_z=1.0):
    """Given x and y coordinates, returns a tuple representing x, y, and z coordinates taken from the grid.

    Interpolates the x and y coordinates
    """

    if x % 1 < y % 1:
        weight_z1 = 1 / sqrt((floor(x) - x) ** 2 + (floor(y) - y) ** 2)
        weight_z2 = 1 / sqrt((floor(x) - x) ** 2 + (ceil(y) - y) ** 2)
        weight_z3 = 1 / sqrt((ceil(x) - x) ** 2 + (ceil(y) - y) ** 2)

        try:
            z1 = grid[np.where(np.all(grid[:, :2] == (floor(x), floor(y)), axis=1))[0][0], 2]
            z2 = grid[np.where(np.all(grid[:, :2] == (floor(x),  ceil(y)), axis=1))[0][0], 2]
            z3 = grid[np.where(np.all(grid[:, :2] == (ceil(x),   ceil(y)), axis=1))[0][0], 2]
        except IndexError:
            return x, y, default_z
    else:
        weight_z1 = 1 / sqrt((floor(x) - x) ** 2 + (floor(y) - y) ** 2)
        weight_z2 = 1 / sqrt((ceil(x) - x) ** 2 + (floor(y) - y) ** 2)
        weight_z3 = 1 / sqrt((ceil(x) - x) ** 2 + (ceil(y) - y) ** 2)

        try:
            z1 = grid[np.where(np.all(grid[:, :2] == (floor(x), floor(y)), axis=1))[0][0], 2]
            z2 = grid[np.where(np.all(grid[:, :2] == (ceil(x),  floor(y)), axis=1))[0][0], 2]
            z3 = grid[np.where(np.all(grid[:, :2] == (ceil(x),   ceil(y)), axis=1))[0][0], 2]
        except IndexError:
            return x, y, default_z

    weight_total = weight_z1 + weight_z2 + weight_z3

    weight_z1 /= weight_total
    weight_z2 /= weight_total
    weight_z3 /= weight_total

    return x, y, weight_z1 * z1 + weight_z2 * z2 + weight_z3 * z3


def bounding_terrain(V, grid_x, grid_y):

    # Add one long side piece
    start = len(V['position']) - 24
    V['position'][start + 0] = (  grid_x,  2*grid_y, 0)
    V['position'][start + 1] = (2*grid_x,  2*grid_y, 0)
    V['position'][start + 2] = (  grid_x, -2*grid_y, 0)
    V['position'][start + 3] = (2*grid_x,  2*grid_y, 0)
    V['position'][start + 4] = (  grid_x, -2*grid_y, 0)
    V['position'][start + 5] = (2*grid_x, -2*grid_y, 0)
    
    V['inTexcoord1'][start + 0] = (0.0,                0.0)
    V['inTexcoord1'][start + 1] = (0.35*grid_x,        0.0)
    V['inTexcoord1'][start + 2] = (0.0,         1.4*grid_y)
    V['inTexcoord1'][start + 3] = (0.35*grid_x,        0.0)
    V['inTexcoord1'][start + 4] = (0.0,         1.4*grid_y)
    V['inTexcoord1'][start + 5] = (0.35*grid_x, 1.4*grid_y)
    
    V['inTexcoord2'][start + 0] = (0.0,             0.0)
    V['inTexcoord2'][start + 1] = (0.5*grid_x,      0.0)
    V['inTexcoord2'][start + 2] = (0.0,        2*grid_y)
    V['inTexcoord2'][start + 3] = (0.5*grid_x,      0.0)
    V['inTexcoord2'][start + 4] = (0.0,        2*grid_y)
    V['inTexcoord2'][start + 5] = (0.5*grid_x, 2*grid_y)
    
    V['inNormal'][start + 0] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 1] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 2] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 3] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 4] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 5] = (0.0, 0.0, 1.0)

    # Add the other long side piece
    start = len(V['position']) - 18
    V['position'][start + 0] = (-grid_x,    2*grid_y, 0)
    V['position'][start + 1] = (-2*grid_x,  2*grid_y, 0)
    V['position'][start + 2] = (-grid_x,   -2*grid_y, 0)
    V['position'][start + 3] = (-2*grid_x,  2*grid_y, 0)
    V['position'][start + 4] = (-grid_x,   -2*grid_y, 0)
    V['position'][start + 5] = (-2*grid_x, -2*grid_y, 0)

    V['inTexcoord1'][start + 0] = (0.0,                0.0)
    V['inTexcoord1'][start + 1] = (0.35*grid_x,        0.0)
    V['inTexcoord1'][start + 2] = (0.0,         1.4*grid_y)
    V['inTexcoord1'][start + 3] = (0.35*grid_x,        0.0)
    V['inTexcoord1'][start + 4] = (0.0,         1.4*grid_y)
    V['inTexcoord1'][start + 5] = (0.35*grid_x, 1.4*grid_y)

    V['inTexcoord2'][start + 0] = (0.0,             0.0)
    V['inTexcoord2'][start + 1] = (0.5*grid_x,      0.0)
    V['inTexcoord2'][start + 2] = (0.0,        2*grid_y)
    V['inTexcoord2'][start + 3] = (0.5*grid_x,      0.0)
    V['inTexcoord2'][start + 4] = (0.0,        2*grid_y)
    V['inTexcoord2'][start + 5] = (0.5*grid_x, 2*grid_y)

    V['inNormal'][start + 0] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 1] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 2] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 3] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 4] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 5] = (0.0, 0.0, 1.0)

    # Add short side piece
    start = len(V['position']) - 12
    V['position'][start + 0] = (+grid_x, 2*grid_y, 0)
    V['position'][start + 1] = (-grid_x, 2*grid_y, 0)
    V['position'][start + 2] = (+grid_x,   grid_y, 0)
    V['position'][start + 3] = (-grid_x, 2*grid_y, 0)
    V['position'][start + 4] = (+grid_x,   grid_y, 0)
    V['position'][start + 5] = (-grid_x,   grid_y, 0)

    V['inTexcoord1'][start + 0] = (0.0,                0.0)
    V['inTexcoord1'][start + 1] = (0.35*grid_x,        0.0)
    V['inTexcoord1'][start + 2] = (0.0,         1.4*grid_y)
    V['inTexcoord1'][start + 3] = (0.35*grid_x,        0.0)
    V['inTexcoord1'][start + 4] = (0.0,         1.4*grid_y)
    V['inTexcoord1'][start + 5] = (0.35*grid_x, 1.4*grid_y)

    V['inTexcoord2'][start + 0] = (0.0,           0.0)
    V['inTexcoord2'][start + 1] = (grid_x,        0.0)
    V['inTexcoord2'][start + 2] = (0.0,    0.5*grid_y)
    V['inTexcoord2'][start + 3] = (grid_x,        0.0)
    V['inTexcoord2'][start + 4] = (0.0,    0.5*grid_y)
    V['inTexcoord2'][start + 5] = (grid_x, 0.5*grid_y)

    V['inNormal'][start + 0] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 1] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 2] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 3] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 4] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 5] = (0.0, 0.0, 1.0)

    # Add other short side piece
    start = len(V['position']) - 6
    V['position'][start + 0] = (+grid_x, -2*grid_y, 0)
    V['position'][start + 1] = (-grid_x, -2*grid_y, 0)
    V['position'][start + 2] = (+grid_x,   -grid_y, 0)
    V['position'][start + 3] = (-grid_x, -2*grid_y, 0)
    V['position'][start + 4] = (+grid_x,   -grid_y, 0)
    V['position'][start + 5] = (-grid_x,   -grid_y, 0)

    V['inTexcoord1'][start + 0] = (0.0,                0.0)
    V['inTexcoord1'][start + 1] = (1.4*grid_x,         0.0)
    V['inTexcoord1'][start + 2] = (0.0,        0.35*grid_y)
    V['inTexcoord1'][start + 3] = (1.4*grid_x,         0.0)
    V['inTexcoord1'][start + 4] = (0.0,        0.35*grid_y)
    V['inTexcoord1'][start + 5] = (1.4*grid_x, 0.35*grid_y)

    V['inTexcoord2'][start + 0] = (0.0,           0.0)
    V['inTexcoord2'][start + 1] = (grid_x,        0.0)
    V['inTexcoord2'][start + 2] = (0.0,    0.5*grid_y)
    V['inTexcoord2'][start + 3] = (grid_x,        0.0)
    V['inTexcoord2'][start + 4] = (0.0,    0.5*grid_y)
    V['inTexcoord2'][start + 5] = (grid_x, 0.5*grid_y)

    V['inNormal'][start + 0] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 1] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 2] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 3] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 4] = (0.0, 0.0, 1.0)
    V['inNormal'][start + 5] = (0.0, 0.0, 1.0)


def get_terrain_program(GRID_X=33, GRID_Y=33, starting_coords=(1.0, 1.5, 2.0, 2.5)):
    GRID_SIZE = GRID_X * GRID_Y

    # Create an array big enough for all of the generated terrain,
    # plus a surrounding flat terrain.
    V = np.zeros(GRID_SIZE + 6*4, [
        ("position", np.float32, 3),
        ("inNormal", np.float32, 3),
        ('inTexcoord1', np.float32, 2),
        ('inTexcoord2', np.float32, 2),
    ])

    # Create a grid and seed the corners with starting values
    heights = np.ones((GRID_X, GRID_Y))
    heights[0,                   0] = starting_coords[0]
    heights[0,          GRID_X - 1] = starting_coords[1]
    heights[GRID_Y - 1,          0] = starting_coords[2]
    heights[GRID_X - 1, GRID_Y - 1] = starting_coords[3]

    fractize(heights, random_factor=GRID_X / 8)

    # Make the whole thing appear to come from the ground
    # by forcing the edges to zero.
    heights[0, :] = 0.0
    heights[:, 0] = 0.0
    heights[heights.shape[0]-1, :] = 0.0
    heights[:, heights.shape[1]-1] = 0.0

    def calc_normal(p1, p2, p3):
        p1 = np.array(p1 + (heights[p1],))
        p2 = np.array(p2 + (heights[p2],))
        p3 = np.array(p3 + (heights[p3],))

        cp = np.cross(p2 - p1, p3 - p1)
        norm = np.linalg.norm(cp)

        return cp / norm if norm != 0 else cp

    for i in range(GRID_X):
        for j in range(GRID_Y):
            V['position'][GRID_X*i + j] = [0.5*(i-GRID_X/2),  0.5*(j-GRID_Y/2), heights[i, j]]

            V['inTexcoord1'][GRID_X*i + j] = np.array([[0.5 * i, 0.5 * j]])
            V['inTexcoord2'][GRID_X*i + j] = np.array([[0.25 * i, 0.25 * j]])

            if 0 < i < GRID_X - 1 and 0 < j < GRID_Y - 1:
                normal = (
                    calc_normal((i, j), (i-1,   j), (i,   j-1)) +
                    calc_normal((i, j), (i-1, j+1), (i-1,   j)) +
                    calc_normal((i, j), (i,   j+1), (i-1, j+1)) +
                    calc_normal((i, j), (i+1,   j), (i,   j+1)) +
                    calc_normal((i, j), (i+1, j-1), (i+1,   j)) +
                    calc_normal((i, j), (i,   j-1), (i+1, j-1))
                ) / 6.0
            elif i == 0 and 0 < j < GRID_Y - 1:
                normal = (
                    calc_normal((i, j), (i,   j+1), (i-1, j+1)) +
                    calc_normal((i, j), (i+1,   j), (i,   j+1)) +
                    calc_normal((i, j), (i+1, j-1), (i+1,   j)) +
                    calc_normal((i, j), (i,   j-1), (i+1, j-1))
                ) / 4.0
            elif i == GRID_X - 1 and 0 < j < GRID_Y - 1:
                normal = (
                    calc_normal((i, j), (i-1,   j), (i,   j-1)) +
                    calc_normal((i, j), (i-1, j+1), (i-1,   j)) +
                    calc_normal((i, j), (i,   j+1), (i-1, j+1))
                ) / 3.0
            elif 0 < i < GRID_X - 1 and j == 0:
                normal = (
                    calc_normal((i, j), (i-1,   j), (i,   j-1)) +
                    calc_normal((i, j), (i-1, j+1), (i-1,   j)) +
                    calc_normal((i, j), (i,   j+1), (i-1, j+1)) +
                    calc_normal((i, j), (i+1,   j), (i,   j+1))
                ) / 4.0
            elif 0 < i < GRID_X - 1 and j == GRID_Y - 1:
                normal = (
                    calc_normal((i, j), (i-1,   j), (i,   j-1)) +
                    calc_normal((i, j), (i+1, j-1), (i+1,   j)) +
                    calc_normal((i, j), (i,   j-1), (i+1, j-1))
                ) / 3.0

            elif i == 0 and j == 0:
                normal = calc_normal((i, j), (i+1,   j), (i,   j+1))

            elif i == GRID_X - 1 and j == 0:
                normal = (
                    calc_normal((i, j), (i-1, j+1), (i-1,   j)) +
                    calc_normal((i, j), (i,   j+1), (i-1, j+1))
                ) / 2.0

            elif i == 0 and j == GRID_Y - 1:
                normal = (
                    calc_normal((i, j), (i+1, j-1), (i+1,   j)) +
                    calc_normal((i, j), (i,   j-1), (i+1, j-1))
                ) / 2.0

            elif i == GRID_X - 1 and j == GRID_Y - 1:
                normal = calc_normal((i, j), (i-1,   j), (i,   j-1))

            else:
                normal = np.array([[0.0, 1.0, 1.0]])

            V['inNormal'][GRID_X*i + j] = normal
    
    bounding_terrain(V, GRID_X/4, GRID_Y/4)
    
    vertices = VertexBuffer(V)

    I = []

    for i in range(GRID_X-1):
        for j in range(GRID_Y-1):
            # First triangle
            I.append(GRID_X*i + j + 0)
            I.append(GRID_X*i + j + 1)
            I.append(GRID_X*i + GRID_Y + j)

            # Second triangle
            I.append(GRID_X*i + j + 1)
            I.append(GRID_X*i + GRID_Y + j + 0)
            I.append(GRID_X*i + GRID_Y + j + 1)

    I += [len(V['position']) - l for l in range(24, 0, -1)]

    # Build program
    # --------------------------------------
    program = Program(vertex, fragment)

    program.bind(vertices)
    program['color'] = 1, 0, 0, 1

    grass1 = imread('grass.jpg')
    program['grass1'] = Texture2D(grass1)
    program['grass1'].wrapping = gl.GL_REPEAT

    grass2 = imread('grass3.jpg')
    program['grass2'] = Texture2D(grass2)
    program['grass2'].wrapping = gl.GL_REPEAT

    ground = imread('ground.bmp')
    program['ground'] = Texture2D(ground)
    program['ground'].wrapping = gl.GL_REPEAT

    program._indices = IndexBuffer(I)
    program._mode = gl.GL_TRIANGLES

    return program, V['position']