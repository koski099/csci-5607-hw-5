import numpy as np
import OpenGL.GL as gl
from vispy.gloo import VertexBuffer, IndexBuffer, Program

vertex = """
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
varying vec4 v_color;

attribute vec3 position;
attribute vec3 color;

void main() {
    //v_color = vec4(0.46, 0.66, 0.07, 1);
    v_color = vec4(color, 1);
    gl_Position = projection * view * model * vec4(position,1.0);
}
"""

fragment = """
varying vec4 v_color;
varying vec3 color;

void main() {
    gl_FragColor = v_color;
}
"""


def grow(word, points, indices, colors, endpoints, base, original_length=None, old_base=None):
    new_base = len(points)
    length = len(word)
    base_point = points[base]

    if not length:
        return

    if original_length is None:
        original_length = length

    letter = word[0]

    ratio = 0.4 * float(length) / original_length

    direction = 1

    if letter == 'a':
        new_point = (
            base_point[0] + direction * -0.5 * ratio,
            base_point[1] + direction * 0.1 * ratio,
            base_point[2] + ratio)

    elif letter == 'b':
        new_point = (
            base_point[0] + direction * 0.4 * ratio,
            base_point[1] + direction * -0.8 * ratio,
            base_point[2] + ratio)

    elif letter == 'c':
        new_point = (
            base_point[0],
            base_point[1] + direction * 0.5 * ratio,
            base_point[2] + ratio)

    elif letter == '[':
        return grow(word[1:], points, indices, colors, endpoints, base, original_length=original_length, old_base=base)

    elif letter == ']':
        return grow(word[1:], points, indices, colors, endpoints, old_base, original_length=original_length, old_base=old_base)

    else:
        print 'lol wut: ', letter
        new_point = (
            base_point[0] + ratio,
            base_point[1] + ratio,
            base_point[2] + ratio)

    indices.append((base, new_base))
    points.append(new_point)

    if len(word) > 1 and word[1] == ']' or len(word) == 1 and word[0] != ']':
        endpoints.append(new_point)

    if len(word) > 1:
        colors.append((1.0, 0.41, 0.71) if word[1] == ']' else (0.6, 0.98, 0.6))

    if len(word) == 1 and word[0] != ']':
        colors.append((1.0, 0.41, 0.71))

    grow(word[1:], points, indices, colors, endpoints, new_base, original_length=original_length, old_base=old_base)


def get_plant_program(start=(0, 0, 0), word='abcabc'):
    points = [start, (start[0], start[1], start[2] + 0.1)]
    endpoints = []
    indices = [(0, 1)]
    colors = [(0.6, 0.98, 0.6)] * 2

    grow(word, points, indices, colors, endpoints, 1)

    V = np.zeros(len(points), [
        ("position", np.float32, 3),
        ("color", np.float32, 3),
    ])

    V["position"] = np.array(points)
    V["color"] = np.array(colors)

    vertices = VertexBuffer(V)

    # Build program
    # --------------------------------------
    program = Program(vertex, fragment)

    program.bind(vertices)

    program._indices = IndexBuffer(np.array(indices))
    program._mode = gl.GL_LINES

    return program, endpoints