from math import floor, ceil, sqrt
import numpy as np
import OpenGL.GL as gl
from vispy.gloo import Program, IndexBuffer, VertexBuffer, Texture2D
from vispy.io import imread
from HW5.fractize import fractize


vertex = """
#version 150 core
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

attribute vec3 position;
attribute vec3 inNormal;
attribute vec2 texuv;

out vec2 texcoord;

void main() {
    gl_Position = projection * view * model * vec4(position, 1.0);
    texcoord = texuv;
}
"""

fragment = """
#version 150 core

uniform sampler2D water;

in vec2 texcoord;

void main() {
   //gl_FragColor = vec4(0.0, 0.0, 1.0, 0.25);
   gl_FragColor = vec4(texture(water, texcoord).rgb, 0.5);
}
"""


def get_water_program(GRID_X=33, GRID_Y=33):
    GRID_X -= 1
    GRID_Y -= 1

    # Create an array big enough for all of the generated terrain,
    # plus a surrounding flat terrain.
    V = np.zeros(6, [
        ("position", np.float32, 3),
        ("texuv", np.float32, 2),
        ("inNormal", np.float32, 3)])

    V['position'][0] = (-GRID_X/4,  GRID_Y/4, -0.1)
    V['position'][1] = (+GRID_X/4,  GRID_Y/4, -0.1)
    V['position'][2] = (-GRID_X/4, -GRID_Y/4, -0.1)
    V['position'][3] = (+GRID_X/4,  GRID_Y/4, -0.1)
    V['position'][4] = (-GRID_X/4, -GRID_Y/4, -0.1)
    V['position'][5] = (+GRID_X/4, -GRID_Y/4, -0.1)
    
    V['texuv'][0] = (0.0, 0.0)
    V['texuv'][1] = (2.0, 0.0)
    V['texuv'][2] = (0.0, 2.0)
    V['texuv'][3] = (2.0, 0.0)
    V['texuv'][4] = (0.0, 2.0)
    V['texuv'][5] = (2.0, 2.0)

    V['inNormal'][0] = (0.0, 0.0, 1.0)
    V['inNormal'][1] = (0.0, 0.0, 1.0)
    V['inNormal'][2] = (0.0, 0.0, 1.0)
    V['inNormal'][3] = (0.0, 0.0, 1.0)
    V['inNormal'][4] = (0.0, 0.0, 1.0)
    V['inNormal'][5] = (0.0, 0.0, 1.0)
    
    vertices = VertexBuffer(V)

    I = list(range(6))

    # Build program
    # --------------------------------------
    program = Program(vertex, fragment)

    program.bind(vertices)

    water = imread('water.jpg')
    program['water'] = Texture2D(water)
    program['water'].wrapping = gl.GL_REPEAT

    program._indices = IndexBuffer(I)
    program._mode = gl.GL_TRIANGLES

    return program