from math import pi, cos, sin
from random import random
import numpy as np
import OpenGL.GL as gl
from vispy.gloo import VertexBuffer, IndexBuffer, Program, Texture2D
from vispy.io import imread

vertex = """
#version 150 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

varying vec4 v_color;

out vec2 texcoord;

attribute vec3 position;
attribute vec2 texuv;

void main() {
    v_color = vec4(1.0, 0.41, 0.71, 1);
    gl_Position = projection * view * model * vec4(position,1.0);
    texcoord = texuv;
}
"""

fragment = """
#version 150 core

in vec2 texcoord;

uniform sampler2D flower;

void main() {
    vec4 color = texture(flower, texcoord).rgba;

    if (color.a < 0.1) {
        discard;
    }

    gl_FragColor = color;
}
"""


def rotate((p1, p2, p3), angle, x, y, z):
    a = cos(angle)*x - sin(angle)*z
    b = y
    c = sin(angle)*x + cos(angle)*z

    return p1 + a, p2 + b, p3 + c


def get_flower_program(flower_points):
    points = []
    indices = []
    texuvs = []

    for i, point in enumerate(flower_points):

        angle = pi/2 * random() - pi/4

        points += [
            rotate(point, angle, -0.1,  0.1, 0.0),
            rotate(point, angle,  0.1,  0.1, 0.0),
            rotate(point, angle, -0.1, -0.1, 0.0),

            rotate(point, angle,  0.1,  0.1, 0.0),
            rotate(point, angle, -0.1, -0.1, 0.0),
            rotate(point, angle,  0.1, -0.1, 0.0),
        ]

        texuvs += [
            (0.0, 0.0),
            (1.0, 0.0),
            (0.0, 1.0),
            (1.0, 0.0),
            (0.0, 1.0),
            (1.0, 1.0),
        ]

        indices.append(tuple(6*i + n for n in range(6)))

    V = np.zeros(len(points), [
        ("position", np.float32, 3),
        ("texuv", np.float32, 2),
    ])

    V["position"] = np.array(points)
    V["texuv"] = np.array(texuvs)

    vertices = VertexBuffer(V)

    # Build program
    # --------------------------------------
    program = Program(vertex, fragment)

    program.bind(vertices)

    ground = imread('flower.png')
    program['flower'] = Texture2D(ground)

    program._indices = IndexBuffer(np.array(indices))
    program._mode = gl.GL_TRIANGLES

    return program