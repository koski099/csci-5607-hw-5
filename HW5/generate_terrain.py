# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Copyright (c) 2014, Nicolas P. Rougier. All rights reserved.
# Distributed under the terms of the new BSD License.
# -----------------------------------------------------------------------------
from random import choice, random
from math import pi, cos, sin
import sys

import numpy as np
import OpenGL.GL as gl
import OpenGL.GLUT as glut
from vispy.util.transforms import perspective, translate, rotate
from HW5.lsystem import build
from HW5.programs.buildings import get_buildings_program
from HW5.programs.flower import get_flower_program

from HW5.programs.plant import get_plant_program
from HW5.programs.sky import get_sky_program
from HW5.programs.tree import get_tree_program
from HW5.programs.terrain import get_terrain_program, interpolate_grid
from HW5.programs.water import get_water_program


# Generating fractals is very recursive
sys.setrecursionlimit(5000)


def display():
    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

    for program in programs:
        gl.glLineWidth(4)
        program.draw(program._mode, program._indices)

    glut.glutSwapBuffers()


def reshape(width, height):
    global projection
    gl.glViewport(0, 0, width, height)
    projection = perspective(45.0, width/float(height), 0.001, 100.0)

    for program in programs:
        program['projection'] = projection


keys = {
    'w': False,
    'a': False,
    's': False,
    'd': False,
    'q': False,
    'e': False,
}


def keyboard(key, x, y):
    if key == '\033':
        sys.exit()
    elif key in keys:
        keys[key] = True
    else:
        print key, x, y


def keyboard_up(key, x, y):
    if key in keys:
        keys[key] = False

old_x, old_y = 0, 0


def motion(x, y, *args, **kwargs):
    global old_x, old_y
    dx, dy = x - old_x, y - old_y
    old_x, old_y = x, y

    angles['x'] += dx
    angles['y'] = min(max(angles['y'] + dy, -90), 90)

    # print x, y, args, kwargs
    # glut.glutWarpPointer(256, 256)


def timer(fps):
    global model, view

    model = np.eye(4, dtype=np.float32)
    # rotate(model, rotation['phi'], 1, 0, 0)
    # rotate(model, rotation['theta'], 0, 1, 0)
    # rotate(model, rotation['psi'], 0, 0, 1)

    view = np.eye(4, dtype=np.float32)
    translate(view, position['x'], position['y'], position['z'])
    rotate(view, angles['x'], 0, 0, 1)
    rotate(view, -90, 1, 0, 0)
    rotate(view, 90, 0, 1, 0)
    rotate(view, angles['y'], 1, 0, 0)

    for program in programs:
        program['model'] = model
        program['view'] = view

    # Handle moving the camera around based on what keys the user has pressed
    if keys['w']:
        position['x'] -= cos(angles['x'] * pi / 180) * 0.2
        position['y'] += sin(angles['x'] * pi / 180) * 0.2
        # position['z'] = interpolate_grid(grid, position['x'], position['y'])[2] - 5

    if keys['s']:
        position['x'] += cos(angles['x'] * pi / 180) * 0.2
        position['y'] -= sin(angles['x'] * pi / 180) * 0.2
        # position['z'] = interpolate_grid(grid, position['x'], position['y'])[2] - 5

    if keys['a']:
        position['x'] -= sin(angles['x'] * pi / 180) * 0.2
        position['y'] -= cos(angles['x'] * pi / 180) * 0.2
        # position['z'] = interpolate_grid(grid, position['x'], position['y'])[2] - 5

    if keys['d']:
        position['x'] += sin(angles['x'] * pi / 180) * 0.2
        position['y'] += cos(angles['x'] * pi / 180) * 0.2
        # position['z'] = interpolate_grid(grid, position['x'], position['y'])[2] - 5

    if keys['q']:
        position['z'] -= 0.05

    if keys['e']:
        position['z'] += 0.05

    glut.glutTimerFunc(1000/fps, timer, fps)
    glut.glutPostRedisplay()


# Glut init
# --------------------------------------
glut.glutInit(sys.argv)
glut.glutInitDisplayMode(glut.GLUT_DOUBLE | glut.GLUT_RGBA | glut.GLUT_DEPTH)
glut.glutCreateWindow('The Park')
glut.glutReshapeWindow(512, 512)
glut.glutReshapeFunc(reshape)
glut.glutKeyboardFunc(keyboard)
glut.glutKeyboardUpFunc(keyboard_up)
glut.glutPassiveMotionFunc(motion)
glut.glutDisplayFunc(display)
glut.glutTimerFunc(1000/60, timer, 60)

# Build program
# --------------------------------------
GRID_X, GRID_Y = 65, 65
terrain, grid = get_terrain_program(GRID_X, GRID_Y, starting_coords=(1.0, 0.0, 0.0, 0.0))

programs = [
    terrain,
    get_buildings_program(GRID_X, GRID_Y),
    #get_sky_program(GRID_X, GRID_Y),
    get_water_program(GRID_X, GRID_Y),
]

flower_points = []

for i in range(100):
    grid_x = 0.5 * (GRID_X - 1) * (random() - 0.5)
    grid_y = 0.5 * (GRID_Y - 1) * (random() - 0.5)
    start = interpolate_grid(grid, grid_x, grid_y)

    if start[2] < 0:
        continue

    productions = {
        'a': '[a][b]c',
        'b': '[ac]',
        'c': 'a'
    }

    word = build(choice('abc'), productions, steps=choice([2, 3]))

    program, endpoints = get_plant_program(start, word)
    flower_points += endpoints
    programs.append(program)

programs.append(get_flower_program(flower_points))


for i in range(10):
    grid_x = 0.5 * (GRID_X - 1) * (random() - 0.5)
    grid_y = 0.5 * (GRID_Y - 1) * (random() - 0.5)
    start = interpolate_grid(grid, grid_x, grid_y)

    if start[2] < 0:
        continue

    productions = {
        'a': '[de]a[bc]',
        'b': '[dc]a[be]',
        'c': '[ce]a[db]',
        'd': '[bd]a[ec]',
        'e': '[eb]a[cd]',
    }

    word = build('a', productions, steps=choice([3, 4]))

    programs.append(get_tree_program(start, word))

# Build view, model, projection & normal
# --------------------------------------
position = {
    'x': 0,
    'y': 0,
    'z': grid[np.where(np.all(grid[:, :2] == (0, 0), axis=1))[0][0], 2] - 5.0,
}

angles = {
    'x': 0,
    'y': 0,
}

rotation = {
    'phi': 0,
    'theta': 0,
    'psi': 0,
}

view = np.eye(4, dtype=np.float32)
model = np.eye(4, dtype=np.float32)
projection = np.eye(4, dtype=np.float32)
translate(view, position['x'], position['y'], position['z'])

for each_program in programs:
    each_program['model'] = model
    each_program['view'] = view

# Build texture
# --------------------------------------
# GLint texAttrib = glGetAttribLocation(shaderProgram, "inTexcoord");
# glEnableVertexAttribArray(texAttrib);
# glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)(3*sizeof(float)));

# OpenGL initalization
# --------------------------------------
gl.glClearColor(0.53, 0.81, 0.92, 1)
gl.glEnable(gl.GL_DEPTH_TEST)
gl.glEnable(gl.GL_LINE_SMOOTH)
gl.glEnable(gl.GL_MULTISAMPLE)
gl.glEnable(gl.GL_BLEND)
gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
# gl.glDepthFunc(gl.GL_GREATER)

# Start
# --------------------------------------
glut.glutMainLoop()
