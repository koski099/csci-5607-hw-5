import numpy as np
from random import random


def grow(word, points, indices, colors, base, original_length=None, old_base=None,
         stem_color=(0.63, 0.32, 0.18), leaf_color=(0.6, 0.98, 0.6)):
    new_base = len(points)
    length = len(word)
    base_point = points[base]

    if not length:
        return

    if original_length is None:
        original_length = length

    letter = word[0]

    ratio = 0.4 * float(length) / original_length

    #direction = -1 if random() < 0.5 else 1
    direction = 1

    if letter == 'a':
        new_point = (
            base_point[0] + direction * -0.5 * ratio,
            base_point[1] + direction * 0.1 * ratio,
            base_point[2] + ratio)
    elif letter == 'b':
        new_point = (
            base_point[0] + direction * 0.4 * ratio,
            base_point[1] + direction * -0.8 * ratio,
            base_point[2] + ratio)
    elif letter == 'c':
        new_point = (
            base_point[0],
            base_point[1] + direction * 0.5 * ratio,
            base_point[2] + ratio)
    elif letter == '[':
        return grow(word[1:], points, indices, colors, base, original_length=original_length, old_base=base, stem_color=stem_color, leaf_color=leaf_color)
    elif letter == ']':
        return grow(word[1:], points, indices, colors, old_base, original_length=original_length, old_base=old_base, stem_color=stem_color, leaf_color=leaf_color)
    else:
        print 'lol wut: ', letter
        new_point = (
            base_point[0] + ratio,
            base_point[1] + ratio,
            base_point[2] + ratio)

    indices.append((base, new_base))
    points.append(new_point)

    if len(word) > 1:
        colors.append(leaf_color if word[1] == ']' else stem_color)

    if len(word) == 1 and word[0] != ']':
        colors.append(leaf_color)

    grow(word[1:], points, indices, colors, new_base, original_length=original_length, old_base=old_base, stem_color=stem_color, leaf_color=leaf_color)


def run():
    points = [(0, 0, 0)]
    indices = []
    colors = [(0, 0, 0)]

    grow('a[a][b][c][b]', points, indices, colors, 0)


if __name__ == '__main__':
    run()