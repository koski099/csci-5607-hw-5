#! /usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Copyright (c) 2014, Nicolas P. Rougier. All rights reserved.
# Distributed under the terms of the new BSD License.
# -----------------------------------------------------------------------------
from random import random
import sys
import numpy as np
import OpenGL.GL as gl
import OpenGL.GLUT as glut
from vispy.gloo import Program, VertexBuffer, IndexBuffer
from vispy.util.transforms import perspective, translate, rotate
from HW5.grow import grow

vertex = """
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
attribute vec3 position;
varying vec4 v_color;
void main()
{
    v_color = vec4(0, 0, 0, 1);
    gl_Position = projection * view * model * vec4(position,1.0);
}
"""

fragment = """
varying vec4 v_color;
void main()
{
    gl_FragColor = v_color;
}
"""


def display():
    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
    program.draw(gl.GL_LINES, indices)
    glut.glutSwapBuffers()


def reshape(width, height):
    global projection
    gl.glViewport(0, 0, width, height)
    projection = perspective(45.0, width / float(height), 0.001, 100.0)
    program['projection'] = projection


def keyboard(key, x, y):
    if key == '\033':
        sys.exit()
    elif key == 'w':
        rotation['phi'] += 5
    elif key == 's':
        rotation['phi'] -= 5
    elif key == 'a':
        rotation['theta'] += 5
    elif key == 'd':
        rotation['theta'] -= 5
    elif key == 'q':
        rotation['psi'] += 5
    elif key == 'e':
        rotation['psi'] -= 5
    else:
        print key, x, y


def timer(fps):
    global model

    model = np.eye(4, dtype=np.float32)
    rotate(model, rotation['phi'], 1, 0, 0)
    rotate(model, rotation['theta'], 0, 1, 0)
    rotate(model, rotation['psi'], 0, 0, 1)
    program['model'] = model

    glut.glutTimerFunc(1000 / fps, timer, fps)
    glut.glutPostRedisplay()


# Glut init
# --------------------------------------
glut.glutInit(sys.argv)
glut.glutInitDisplayMode(glut.GLUT_DOUBLE | glut.GLUT_RGBA | glut.GLUT_DEPTH)
glut.glutCreateWindow('Rotating Cube')
glut.glutReshapeWindow(512, 512)
glut.glutReshapeFunc(reshape)
glut.glutKeyboardFunc(keyboard)
glut.glutDisplayFunc(display)
glut.glutTimerFunc(1000 / 60, timer, 60)

# Build cube data
# --------------------------------------
points = [(0, 0, 0)]
indices = []

grow('a[a][b][b][b][caabc]', points, indices, 0)

V = np.zeros(len(points), [
    ("position", np.float32, 3),
])

V["position"] = np.array(points)

vertices = VertexBuffer(V)

indices = IndexBuffer(np.array(indices))

# Build program
# --------------------------------------
program = Program(vertex, fragment)
program.bind(vertices)

# Build view, model, projection & normal
# --------------------------------------
rotation = {
    'phi': 0,
    'theta': 0,
    'psi': 0,
}

view = np.eye(4, dtype=np.float32)
model = np.eye(4, dtype=np.float32)
projection = np.eye(4, dtype=np.float32)
translate(view, 0, 0, -15)
program['model'] = model
program['view'] = view

# OpenGL initalization
# --------------------------------------
gl.glClearColor(1, 1, 1, 1)
gl.glEnable(gl.GL_DEPTH_TEST)

# Start
# --------------------------------------
glut.glutMainLoop()
