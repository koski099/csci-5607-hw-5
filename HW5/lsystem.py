

def build(axiom, productions, steps=3):
    if steps == 0:
        return axiom
    else:
        new_axiom = ''.join(letter if letter in '[]' else productions.get(letter, '!') for letter in axiom)
        return build(new_axiom, productions, steps - 1)


def run():
    productions = {
        'a': 'ab',
        'b': 'a'
    }

    for i in range(6):
        print build('b', productions, steps=i)


if __name__ == '__main__':
    run()